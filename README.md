# Practica de Big Data Processing

## 1.- Usar kafka-console-producer.sh y kafka-console-consumer.sh para leer un fichero json

Los pantallazos de los comandos y el resultado están en el siguiente Pdf :

https://drive.google.com/open?id=1TG8ewJO8xdbGCIrzi316zhNnvevWrOaR

## 2.- Crear un código Scala para que lea el fichero json y filtre 2 palabras del fichero

### Solución A

En un terminal he hecho lo siguiente :

bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic PracticaKafka2

cat bin/personalcop.json | bin/kafka-console-producer.sh --broker-list localhost:9092 --topic PracticaKafka2

Luego desde IntelliJ he ejecutado el fichero PrKafkaJson sobre el fichero personalcop.json

***PrKafkaJson ver aqui*** : https://drive.google.com/file/d/1M4p3eVi4laBVynRrK0WgiCGbo8Z0Lc4k/view?usp=sharing

***Fichero personalcop.json ver aqui*** : https://drive.google.com/file/d/1d3_FX91PuZ_V5JBHg05LwnM0LKE3BJiQ/view?usp=sharing

En elcódigo scala he creado un schema para poner los datos por columnas y poder consultarlos pero al filtrar me filtra el registro completo
y no he conseguido que me filtre sólo las palabras. He buscado por internet pero no he dado con la solución.
He filtrado los registros que contengan sexo = "Male" y que contengan Last_name = "Bea".

### Solución B

He hecho otro código en scala que lee un fichero json en el directorio /home/keepcoding/Practica/personaljson.json y si filtra sólo las palabras
pero no lo hace por streaming. Filtra por las palabras "Female" y "Bea".

***El código PrLeerPersonal ver aqui*** : https://drive.google.com/file/d/1dWrXs3KprVNGcQATkWSTsWecqgSQn8f3/view?usp=sharing

***El fichero personaljson.json ver aqui*** : https://drive.google.com/file/d/1J7GYTf1ffb7z56eTEUsPOllb1gwixF8Q/view?usp=sharing

***El pantallazo ver aqui*** : https://drive.google.com/file/d/1pJN60d1rAT6_ega3T3DEShpIBjBjqiXc/view?usp=sharing

### Solución C

Al final he hecho un un productor en scala (Productor.scala) que transmite las líneas del fichero personaljson.json y 
las consume con el consumidor en scala (PrFiltraPalabras) que filtra las palabras "Male" y "Bea", aunque el filtrado es un poco tosco.

***El código Productor ver aqui*** : https://drive.google.com/file/d/1zfCwp4yXboAf_-fpArLjuKaHD5EdP_aq/view?usp=sharing

***El código PrFiltraPalabras ver aquí*** : https://drive.google.com/file/d/1AseI_GY3I0C74hkspTO4-m7j4ghGHlnY/view?usp=sharing

***El fichero personaljson.json ver aquí*** : https://drive.google.com/file/d/1J7GYTf1ffb7z56eTEUsPOllb1gwixF8Q/view?usp=sharing

***El pantallazo ver aquí*** : https://drive.google.com/file/d/1LU4IzVkjuHvBmlWqiUQoDOIoQxqf3d_z/view?usp=sharing



